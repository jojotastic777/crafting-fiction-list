# Crafting Fiction List

A list of all the "crafting" fiction, original or fanfiction, that I can find.

## Actual Books (Like, that you can buy)
- Factory of the Gods, by Alex Raizman: https://www.goodreads.com/series/303694-factory-of-the-gods (There are 2 books in this series that aren't on the goodreads series page.)

## Original Fiction
- The Way Ahead, by NorskDaedalus: https://www.royalroad.com/fiction/42202/the-way-ahead
- The Runesmith, by Kuropon: https://www.royalroad.com/fiction/33844/the-runesmith (Writing isn't the greatest IMO, but the crafting itself is pretty good. Went in a kind of strange direction in the latest chapter.)
- Chaotic Craftsman Worships the Cube, by ProbablyATurnip: https://www.royalroad.com/fiction/41656/chaotic-craftsman-worships-the-cube
- Automata Prime, by Xian: https://www.royalroad.com/fiction/33577/i-was-reborn-into-a-fantasy-world-as-a-magic-robot (This one's not very good IMO)
- Magic-Smithing, by kosnik4: https://www.royalroad.com/fiction/31474/magic-smithing
- Auntie toasts the VRMMORPG, by Joan_Ninja_Hen: https://www.royalroad.com/fiction/26135/auntie-toasts-the-vrmmorpg (This one's kind of strange)
- Age of Gods - A VRMMO Story, by thomasdarkrose: https://www.royalroad.com/fiction/14768/age-of-gods-a-vrmmo-story
- The Power of Formations, by em_rez: https://www.royalroad.com/fiction/21450/the-power-of-formations
- Gray Mage: The Alchemist, by Seratar: https://www.royalroad.com/fiction/27784/gray-mage-the-alchemist
- 40 Milleniums of Cultivation, by The Enlightened Master Crouching Cow: https://www.webnovel.com/book/forty-millenniums-of-cultivation_6838665402001705 (I've not read this one.)
- Death March, by AINANA Hiro: https://www.sousetsuka.com/p/blog-page_15.html (I've not read this one.)
- Upgrade Specialist in Another World, by Endless Sea of Clouds: https://www.wuxiaworld.com/novel/upgrade-specialist-in-another-world (I've not read this one.)
- The Legendary Mechanic, by Qi Peijia: https://www.webnovel.com/book/the-legendary-mechanic_8662546605001405 (I've not read this one.)
- The Mech Touch, by Exlor: https://www.webnovel.com/book/the-mech-touch_10636300105085505 (I've not read this one.)

## Fanfiction
- Dying Light, by Darkw01fie: https://www.fanfiction.net/s/13311932 (WARNING: Major Character Death in the first few chapters.)
- Harry Potter and the Artificer Legacy, by Kai_zero: https://archiveofourown.org/works/27353677
- Scientia Weaponizes The Future, by TaliesinSkye: https://forums.sufficientvelocity.com/threads/scientia-weaponizes-the-future.82203/
- Trailblazer, by 3ndless: https://forums.spacebattles.com/threads/trailblazer-worm-gundam-au.680881/
- A Ghost of a Chance, by cliffc999: https://forums.spacebattles.com/threads/a-ghost-of-a-chance-worm-v1-cyoa-alt-taylor.766498/
- Memories of Iron, by becuzitswrong: https://www.fanfiction.net/s/10230499/1/Memories-of-Iron (Rest In Peace, Author.)
- Inspired Voyage, by SIDoragon: https://forums.spacebattles.com/threads/inspired-voyage-st-voyager-si.791005/
- Harry Potter and the Rune Stone Path, by Temporal Knight: https://www.fanfiction.net/s/11898648/1/
- RuneMaster, by Tigerman: https://www.fanfiction.net/s/5077573/1/
- Technology Will Win The Day, by sun tzu: https://forums.spacebattles.com/threads/technology-will-win-the-day-worm-cyoa-si-complete.327745/ (Complete!!!)

### Celestial Forge (Non-NSFW)
- Brockton's Celestial Forge, by LordRoustabout: https://forums.sufficientvelocity.com/threads/brocktons-celestial-forge-worm-jumpchain.70036 (The OG)
- Celestial's Entertainment, by Xolsis: https://forums.sufficientvelocity.com/threads/celestials-entertainment-worm-celestial-forge.84491/
- Conjunction, by GilGilMashi: https://forums.spacebattles.com/threads/conjunction-worm-jumpchain-celestial-forge-variant.960930/ (Not very long yet)
- Dragon-Forged Chains, by Riversand: https://forums.sufficientvelocity.com/threads/dragon-forged-chains-dragon-jumpchain-w-celestial-forge.84249/ (Slow to update)
- Forging Ahead, by Belvoli0: https://forums.sufficientvelocity.com/threads/forging-ahead-a-celestial-forge-wormfic.91555/
- Junk Emporium, by Melakias: https://forums.sufficientvelocity.com/threads/junk-emporium-my-hero-academia-the-celestial-forge-complete.82039 (Actually complete, with ongoing sequel!)
- Junk Head, by Melakias: https://forums.sufficientvelocity.com/threads/junk-head-worm-junk-emporioum-celestial-forge.91322 (Sequel to above)
- Missy's Celestial Brother, by RexHeller: https://forums.sufficientvelocity.com/threads/missys-celestial-brother-worm-si.88853 (Slow to update)
- SOTSOG, by Erthael: https://forums.spacebattles.com/threads/sotsog-worm-celestial-forge.930385/ (Hiatus?)
- Sparky and the Celestial Forge, by Cyco: https://forums.sufficientvelocity.com/threads/sparky-and-the-celestial-forge.94153/
- The Ringing of a Forge of Stars, by Slider Zero: https://forum.questionablequesting.com/threads/the-ringing-of-a-forge-of-stars-warhammer-40k-celestial-forge-v3-oc-ish.14650/ (Despite being on QQ, not actually NSFW)
- Under the Mountain, by ADeshantis: https://forums.spacebattles.com/threads/under-the-mountain-young-justice-celestial-forge.964117/
- (A bunch of dead/sleeping stories I'm not going to link here)

### Tinker of Fiction
- Tinker of Fiction, by MonkPenguin: https://forums.sufficientvelocity.com/threads/tinker-of-fiction-worm-sort-of-crossover.54982/ (The OG)
- In Nuclear Fire, by Poliamida: https://forums.sufficientvelocity.com/threads/in-nuclear-fire.67755/
- Kathy's Brockton Bay Adventures, by Hydralisk: https://forums.sufficientvelocity.com/threads/kathys-brockton-bay-adventures-worm-tinker-of-fiction.67183/
- Get Away from Brockton Bay!, by Kejmur: https://forums.sufficientvelocity.com/threads/get-away-from-brockton-bay-tinker-of-fiction-si.66835/ (Dead/sleeping, not very long)
- I want a refund!, by Unders: https://forums.sufficientvelocity.com/threads/i-want-a-refund-tinker-of-fiction-semi-si.84482/